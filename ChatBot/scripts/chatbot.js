﻿
var acc = "";
var zip = "";
var pubCode = "";
var hasAskedPub = false;
var arrQuestion = new Array();
var tmpString = "";
var botName = "ESP";


var linkForFAQ = "";
var linkForSubStatus = "";
var linkForNew = "";
var linkForComp = "";
var linkForRenew = "";
var linkForBillPay = "";
var linkForUpdate = "";
var linkForCNew = "";
var linkForCRenew = "";
var linkForCUpdate = "";
var linkForBackIssues = "";
var linkForGift = "";
var linkForDigital = "";

var csEmailAddr = "";
var callHours = "";
var csRegTelephone = "";
var cS800Telephone = "";
var csRegFax = "";

var navColor = "";

var holdpopup = false;


(function ($) {
    $(document).ready(function () {
        var $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title'),
            $chatboxTitleClose = $('.chatbox__title__close');

        //if (pubCode == null || pubCode.trim() == "")
        //    pubCode = getUrlVars()["PC"];

        if (pubCode == null)
            pubCode = "";

        $chatbox.hide();

        callToGetPubCode();

        callAPI_Pub_Initial(pubCode);
        if (pubCode != "")
            GetPubURL(pubCode);

        if (pubCode == "") {
            $chatbox.show();

            setTimeout(function () {
                $chatbox.removeClass('chatbox--tray');
                $('.blocker').hide(800);

                if ($(".chatbox__body").html().trim() == "") {
                    $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                                    + "Hi, I am the " + botName + " AI Chatbot."
                                    + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

                    $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));


                    setTimeout(function () {
                        $(".chatbox__body").append("<div class='loading' style='font-size: 11px; text-align: left; display: inline'>"
                            + "<img src='http://espdev.espcomp.net/Chatbot/images/chatLoad.gif' class='loading' /></div>"
                        );
                        $(".loading").last().show();


                        setTimeout(function () {
                            $(".loading").last().hide();
                            $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                                        + "Can I help you?"
                                        + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

                            $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));
                        }, 1500);
                    }, 500);

                }
                $(".chatbox__message").focus();

                setTimeout(function () {
                    if (!holdpopup) {
                        $chatbox.addClass('chatbox--tray');
                        setTimeout(function () {
                            $('.blocker').show(800);
                        }, 800);
                    }
                }, 5000);
            }, 1000);
        }

        $chatboxTitle.on('click', function () {
            holdpopup = true;
            var res = $chatbox.toggleClass('chatbox--tray');

            if ($chatbox.hasClass("chatbox--tray"))
                $('.blocker').show(800);
            else
                $('.blocker').hide(800);

            $(".chatbox__message").focus();
        });

        $chatboxTitleClose.on('click', function (e) {
            e.stopPropagation();
            $chatbox.addClass('chatbox--closed');
        });

        $chatbox.on('transitionend', function () {
            if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
            
        });


        $(".chatbox__message").on('click', function () {
            holdpopup = true;
        });
        $(".chatbox__message").on('keypress', function () {
            holdpopup = true;
        });

    });
})(jQuery);



$(function () {
    $("#usermsg").keypress(function (e) {
        if (e.which == 13 && $(this).val() != "") {
            
            prepareAPICall($(this).val());
            $(this).val("");
            e.preventDefault();
        }
        else if (e.which == 13)
            e.preventDefault();
    });
});


function prepareAPICall(userQuestion) {
    $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--left new_message'><p>"
                + userQuestion
                + "</p></div><div style='font-size: 11px; text-align: right; margin: 0 5px 15px 5px;'>You</div>");

    $(".chatbox__body").append("<div class='loading' style='font-size: 11px; text-align: left; display: inline'>"
        + "<span>" + botName + " CHATBOT</span><img src='http://espdev.espcomp.net/Chatbot/images/chatLoad.gif' class='loading' /></div>"
        );

    $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));

    $(".loading").last().show();

    if (userQuestion.trim().toLowerCase() == "k")
        userQuestion = "ok";
    if (userQuestion.trim().toLowerCase() == "y")
        userQuestion = "yes";
    if (userQuestion.trim().toLowerCase() == "n")
        userQuestion = "no";

    arrQuestion.push(userQuestion);

    if (hasAskedPub) {
        callAPI_Pub(userQuestion);
    }
    else {
        callAPI(userQuestion);
    }
    
    $("p").removeClass("new_message");
}


function callAPI_ForDisplay(value) {
    value = value.replace(/#/ig, "no");

    URL = "https://espdev.espcomp.net/ESPChatbot/GetAnswer?userQuestion=" + value + "&callback=callbackFunc_ForDisplay";
    $.ajax({
        async: true,
        url: URL,
        type: 'GET',
        callback: 'callbackFunc_ForDisplay',
        dataType: 'jsonp',
    });
}

function callbackFunc_ForDisplay(response) {
    var message = response.answers[0].answer;

    if (response.answers[0].questions[0].trim().toLowerCase() == "esp_confirmpublication") {
        var splitPub = tmpString.split('_');
        message = message.replace(/esp_pubcd/ig, splitPub[0]);
        message = message.replace(/esp_pubnm/ig, splitPub[1]);
    }

    $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                                + message
                                + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

    $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));
    $(".loading").hide();
}

function callAPI(value) {
    value = value.replace(/#/ig, "no");

    URL = "https://espdev.espcomp.net/ESPChatbot/GetAnswer?userQuestion=" + value + "&callback=callbackFunc";
    $.ajax({
        async: true,
        url: URL,
        type: 'GET',
        callback: 'callbackFunc',
        dataType: 'jsonp',
    });
}


function callbackFunc(data) {
    addToChatbox(data);
}


function callToGetPubCode() {
    URL = "https://www.pubservice.com/EspApi/ChatBot/GetPubCode";
    $.ajax({
        async: false,
        url: URL,
        type: 'GET',
        dataType: 'json',
        success: function (result) {
            pubCode = result;
        }
    });
}

//Dummy API call
function callAPI_Pub_Initial(value) {
    if (value != "")
        value = "pub_" + value + "_";

    URL = "https://espdev.espcomp.net/ESPChatbot/GetPublication?pubname=" + value + "&callback=callbackFunc_Pub_Initial";
    $.ajax({
        async: false,
        url: URL,
        type: 'GET',
        callback: 'callbackFunc_Pub_Initial',
        dataType: 'jsonp',
    });
}
function callbackFunc_Pub_Initial(data) {
    var splitPub = data.answers[0].answer.split('_');
    if (splitPub != "No good match found in KB.") {
        botName = splitPub[1];
    }
    else {

    }
}

function callAPI_Pub(value) {
    value = value.replace(/#/ig, "no");

    URL = "https://espdev.espcomp.net/ESPChatbot/GetPublication?pubname=" + value + "&callback=callbackFunc_Pub";
    $.ajax({
        async: true,
        url: URL,
        type: 'GET',
        callback: 'callbackFunc_Pub',
        dataType: 'jsonp',
    });
}

function callbackFunc_Pub(data) {
    
    var lastQuestion = arrQuestion[arrQuestion.length - 2];
    var splitPub = data.answers[0].answer.split('_');

    if (splitPub == "No good match found in KB.") {
        arrQuestion.splice(arrQuestion.length - 1, 1);
        callAPI_ForDisplay("ESP_PublicationNotFound");

        $(".loading").hide();
    }
    else {
        pubCode = splitPub[0];
        hasAskedPub = false;

        tmpString = data.answers[0].answer;
        callAPI_ForDisplay("ESP_ConfirmPublication");

        $(".loading").hide();
    }
}

function GetPubURL(value) {
    URL = "https://espdev.espcomp.net/ESPChatbot/GetUrlDetails?pubname=" + value + "&callback=callbackFunc_GetPubURL";

    $.ajax({
        async: false,
        url: URL,
        type: 'GET',
        callback: 'callbackFunc_GetPubURL',
        dataType: 'jsonp',
    });
}


function callbackFunc_GetPubURL(data) {
    
    if (data != null && data.linkForFAQ != null) {
        linkForFAQ = data.linkForFAQ;
        linkForSubStatus = data.linkForSubStatus;
        linkForNew = data.linkForNew;
        linkForComp = data.linkForComp;
        linkForRenew = data.linkForRenew;
        linkForBillPay = data.linkForBillPay;
        linkForUpdate = data.linkForUpdate;
        linkForCNew = data.linkForCNew;
        linkForCRenew = data.linkForCRenew;
        linkForCUpdate = data.linkForCUpdate;
        linkForBackIssues = data.linkForBackIssues;
        linkForGift = data.linkForGift;
        linkForDigital = data.linkForDigital;

        csEmailAddr = data.csEmailAddr;
        callHours = data.callHours;;
        csRegTelephone = data.csRegTelephone;
        cS800Telephone = data.cS800Telephone;
        csRegFax = data.csRegFax;

        navColor = data.navColor;
    }

    //var PubInURL = getUrlVars()["PC"];
    //if (PubInURL != null && navColor != null && navColor != "")
    //    $('.chatbox__title').css('background-color', navColor);

    var $chatbox = $('.chatbox');
    $chatbox.show();

    setTimeout(function () {
        $chatbox.removeClass('chatbox--tray');
        $('.blocker').hide(800);

        if ($(".chatbox__body").html().trim() == "") {
            $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                            + "Hi, I am the " + botName + " AI Chatbot."
                            + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

            $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));


            setTimeout(function () {
                $(".chatbox__body").append("<div class='loading' style='font-size: 11px; text-align: left; display: inline'>"
                    + "<img src='http://espdev.espcomp.net/Chatbot/images/chatLoad.gif' class='loading' /></div>"
                );
                $(".loading").last().show();


                setTimeout(function () {
                    $(".loading").last().hide();
                    $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                                + "Can I help you?"
                                + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

                    $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));
                }, 1500);
            }, 500);

        }
        $(".chatbox__message").focus();

        setTimeout(function () {
            if (!holdpopup) {
                $chatbox.addClass('chatbox--tray');
                setTimeout(function () {
                    $('.blocker').show(800);
                }, 800);
            }
        }, 5000);
    }, 1000);

    
    if (arrQuestion.length > 0)
        callAPI(arrQuestion[arrQuestion.length - 2]);
    else
        $(".loading").hide();
}

function addToChatbox(response) {
    
    var result = response.answers[0].answer;
    var pubcodeIndex = result.toLowerCase().indexOf("pc=pubcode");

    if (result == "No good match found in KB.") {
        result = "Sorry I couldn't understand your question";
        if (pubCode != null && pubCode != "" && linkForFAQ != "")
            result += "<br />Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";

        arrQuestion.splice(arrQuestion.length - 1, 1);
    }
    else if (response.answers[0].source.trim().toLowerCase() != "editorial" ||
            (pubcodeIndex <= -1
                && response.answers[0].questions[0].trim().toLowerCase() != "esp_confirmpublication"
                && response.answers[0].questions[0].trim().toLowerCase() != "esp_ask_publication"
                && result.toLowerCase().indexOf("<contactinfo>") <= -1
                && result.toLowerCase().indexOf("pubcode") <= -1)
    ) {

        arrQuestion.splice(arrQuestion.length - 1, 1);
    }

    if (pubCode == null)
        pubCode = "";

    if (pubCode != null && pubCode.trim() != "") {
        result = result.replace(/pubCode/ig, pubCode);
    }

    result = result.replace(/yourAccNo/ig, $.trim(acc));
    result = result.replace(/yourZip/ig, $.trim(zip));

    if (
        (
            (result.toLowerCase().indexOf("pubcode") != -1 && pubCode.trim() == "")
            || (pubCode.trim() == "" && arrQuestion.length > 0)
        )
        && !hasAskedPub
        && result.toLowerCase() != "sorry i couldn't understand your question"
        ) {
        callAPI("ESP_Ask_Publication");
        hasAskedPub = true;

        $(".loading").hide();
    }
    else if (result.trim().toLowerCase() == "yes" && arrQuestion.length > 0) {
        if (pubCode != null && pubCode != "" && (linkForFAQ == null || linkForFAQ == "")) {
            GetPubURL(pubCode);
        }
        else
            callAPI(arrQuestion[arrQuestion.length - 2]);

    }
    else if (result.trim().toLowerCase() == "no" && arrQuestion.length > 0) {
        callAPI_ForDisplay("ESP_WrongPublication");

        hasAskedPub = true;
        arrQuestion.splice(arrQuestion.length - 1, 1);
    }
    else if (pubCode != null && pubCode != "") {
        if (result.toLowerCase().indexOf("linkforfaq") != -1) {
            result = result.replace(/linkForFAQ/ig, $.trim(linkForFAQ));
        }
        else if (result.toLowerCase().indexOf("linkforsubstatus") != -1) {
            result = result.replace(/linkForSubStatus/ig, $.trim(linkForSubStatus));
            if (linkForSubStatus == null || linkForSubStatus == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkfornew") != -1) {
            result = result.replace(/linkForNew/ig, $.trim(linkForNew));
            if (linkForNew == null || linkForNew == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforcomp") != -1) {
            result = result.replace(/linkForComp/ig, $.trim(linkForComp));
            if (linkForComp == null || linkForComp == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforrenew") != -1) {
            result = result.replace(/linkForRenew/ig, $.trim(linkForRenew));
            if (linkForRenew == null || linkForRenew == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforbillpay") != -1) {
            result = result.replace(/linkForBillPay/ig, $.trim(linkForBillPay));
            if (linkForBillPay == null || linkForBillPay == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforupdate") != -1) {
            result = result.replace(/linkForUpdate/ig, $.trim(linkForUpdate));
            if (linkForUpdate == null || linkForUpdate == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforcnew") != -1) {
            result = result.replace(/linkForCNew/ig, $.trim(linkForCNew));
            if (linkForCNew == null || linkForCNew == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforcrenew") != -1) {
            result = result.replace(/linkForCRenew/ig, $.trim(linkForCRenew));
            if (linkForCRenew == null || linkForCRenew == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforcupdate") != -1) {
            result = result.replace(/linkForCUpdate/ig, $.trim(linkForCUpdate));
            if (linkForCUpdate == null || linkForCUpdate == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkforbackissues") != -1) {
            if (linkForBackIssues == null || linkForBackIssues == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
            else if (linkForBackIssues.toLowerCase().indexOf("http") != -1) {
                var strURL = "https://www.pubservice.com/linkForBackIssues?PC=" + pubCode;
                result = result.replace(strURL, $.trim(linkForBackIssues));
            }
            else
                result = result.replace(/linkForBackIssues/ig, $.trim(linkForBackIssues));

        }
        else if (result.toLowerCase().indexOf("linkforgift") != -1) {
            result = result.replace(/linkForGift/ig, $.trim(linkForGift));
            if (linkForGift == null || linkForGift == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("linkfordigital") != -1) {

            result = result.replace(/linkForDigital/ig, $.trim(linkForDigital));
            if (linkForDigital == null || linkForDigital == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }
        else if (result.toLowerCase().indexOf("<contactinfo>") != -1) {
            var strContact = "<b>" + csRegTelephone + "</b>";

            if (cS800Telephone != null && cS800Telephone != "")
                strContact += " or <b>" + cS800Telephone + "</b>";

            if (callHours != null && callHours != "")
                strContact += " " + callHours;

            if (csRegFax != null && csRegFax != "")
                strContact += "<br /><b>Fax: </b>" + csRegFax;

            if (csEmailAddr != null && csEmailAddr != "")
                strContact += "<br /><b>Email: </b>" + csEmailAddr;

            var strToReplace = "<contactInfo><" + pubCode + ">";
            result = result.replace(strToReplace, $.trim(strContact));
            if (csRegTelephone == null || csRegTelephone == "")
                result = "Sorry, that is not available. Please <a href='https://www.pubservice.com/" + linkForFAQ + "?PC=" + pubCode + "' target='_blank'>click here</a> for more information";
        }

        if (result.toLowerCase() == "no")
            result = "no problem";
        else if (result.toLowerCase() == "yes")
            result = "Please tell me";

        $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                                        + result
                                        + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

        $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));
        $(".loading").hide();
    }
    else {
        if (result.toLowerCase() == "no")
            result = "no problem";
        else if (result.toLowerCase() == "yes")
            result = "Please tell me";

        $(".chatbox__body").append("<div class='chatbox__body__message chatbox__body__message--right new_message'><p>"
                                        + result
                                        + "</p></div><div style='font-size: 11px; text-align: left; margin: 0 35px 15px 5px;'>" + botName + " CHATBOT</div>");

        $('.chatbox__body').scrollTop($('.chatbox__body').prop("scrollHeight"));
        $(".loading").hide();
    }

}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getDetails() {
    acc = $(".expForm_acc").last().val();
    zip = $(".expForm_zip").last().val();
    var isValid = true;

    $(".expForm_acc_err").last().hide();
    $(".expForm_zip_err").last().hide();

    if (acc.trim() == "") {
        $(".expForm_acc_err").last().html("Please fill in your Acc #");
        $(".expForm_acc_err").last().show();
        isValid = false;
    }

    if (zip.trim() == "") {
        $(".expForm_zip_err").last().html("Please fill in your Zip");
        $(".expForm_zip_err").last().show();
        isValid = false;
    }

    if (!isValid)
        return;

    acc = acc.trim();
    zip = zip.trim();

    //Account validation
    if (acc.length == 8) {  //e.g. AE123456
        if (pubCode.trim() == "")
            pubCode = acc.substring(0, 2);

        acc = acc.substring(2, acc.length);
    }
    else if (acc.length == 7) { //e.g. 123456X
        acc = acc.substring(0, acc.length - 1);
    }
    else if (acc.length == 9) { //e.g. AE123456X
        if (pubCode.trim() == "")
            pubCode = acc.substring(0, 2);

        acc = acc.substring(2, acc.length);
        acc = acc.substring(0, acc.length - 1);
    }

    acc = acc.trim();
    if (isNaN(acc) || acc.length != 6 || (pubCode.trim() != "" && pubCode.trim().length != 2)) {
        $(".expForm_acc_err").last().html("Invalid Acc #");
        $(".expForm_acc_err").last().show();
        return;
    }

    if (zip.length < 5) {
        $(".expForm_zip_err").last().html("Invalid Zip");
        $(".expForm_zip_err").last().show();
        return;
    }
    
    callAPI("ESP_ShowExpiry");

}